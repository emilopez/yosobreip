<!--
.. title: whoami
.. slug: whoami
.. date: 2020-11-23 17:50:06 UTC-03:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

```bash
$whoami
emiliano
```

Soy Emiliano López, vivo en Santa Fe, Argentina. 

Me recibí de Ing. en Informática en la Facultad de Ingeniería y Ciencias Hídricas de la Universidad Nacional del Litoral, donde luego hice una Maestría en Computación Aplicada a la Cienca e Ingeniería. Actualmente estoy haciendo el Doctorado en Ingeniería, mención Hidrología. 

Soy profe de *Fundamentos de Programación* y de *Métodos Numéricos y Computación* en la misma facultad. 

Trabajo en el Centro de Estudios Hidroambientales (CENEHA) donde fundamentalmente desarrollo dispositivos de medición con los que registro variables agro-hidrológicas-climáticas y luego proceso para estudiar distintos fenómenos. 

Laburo fundamentalmente con Python, C++ y Arduino y mi stack para datascience está compuesto por Jupyter, Numpy, Pandas y Plotly.

Me podés contactar por telegram a ``@EmiLopez``, por mail a ``emiliano[dot]lopez[AT]gmail[dot]com`` o por twitter a [@yosobreip](https://twitter.com/yosobreip)

