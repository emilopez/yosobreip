.. title: Volviendo al ruedo
.. slug: volviendo-al-ruedo
.. date: 2020-11-22 22:56:25 UTC-03:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text

Decidí volver a tener un blog. Chiquito, humilde, donde describo lo que aprendo y disfruto haciendo. El perfil será geek/nerd, como el de toda persona de bien. Decicí usar gitlab pages para hostearlo, de modo que ante una catástrofe inesperada estas ínfimas ideas perdurarán eternamente en el tiempo. 

La plataforma que usaré para el blog es `Nikola <https://getnikola.com/>`_. No solo porque está hecha por el admirado `Roberto Alsina <https://ralsina.me>`_., sino porque me resultó bastante sencillo hacerlo andar, siguiendo el `getting started <https://getnikola.com/getting-started.html>`_ todo fluyó sin inconvenientes. Cuenta con características que lo hacen especialmente atractivo a mi gusto: podés escribirlo en markdown, rst o jupyter notebooks. 

Lo iré descubriendo con el uso.

Ahora bien, ¿cómo montar tu blog usando exactamente esto mismo?

- Crear un proyecto en gitlab, la URL de la web será: ``usuario.gitlab.io/nombre_del_proyecto``
- Subir un archivo llamado ``.gitlab-ci.yml`` con este contenido (o similar):

::

    pages:
      stage: deploy
      script:
        - mkdir .public
        - cp -r blog/* .public
        - mv .public public
      artifacts:
        paths:
          - public
      only:
        - master

El archivo previo indica a gitlab qué hacer cada vez que pushees contenido a tu repositorio, en este caso, expondrá a la web lo que tengas en el directorio ``blog``. Te clonás el repositorio a un directorio local, ya en un rato vamos a usarlo.

Ahora instalamos Nikola en la máquina, nos paramos en el directorio deseado y hacemos:

.. code-block:: bash

    $ python3 -m venv nikola
    $ cd nikola
    $ source bin/activate
    $ bin/python -m pip install -U pip setuptools wheel
    $ bin/python -m pip install -U "Nikola[extras]"

Con lo anterior habremos creado un entorno virtual y ahí dentro Nikola. Ahora nos queda activar el entorno y comenzar generando el blog para luego continuar con los posts. 

::

    $nikola init --demo blog

El comando previo te irá pidiendo información que cargas por única vez. Ahora generamos el sitio haciendo:

::

    $nikola build

Una vez hecho esto tendrás el contenido estatico del blog bajo el directorio ``output``. Ese contenido es el que hay que subir a gitlab. Pero aguantá, veamos como crear nuestros posts:

::

    $nikola new_post -e

Esto lo que generará es un archivo en el directorio ``posts``, que podés modificar con el editor de texto que prefieras. Si tenés seteado en la variable de entorno EDITOR el programa que utilizás, entonces se abrirá automáticamente para que escribas. Una forma temporal de setear tu variable es haciendo ``export EDITOR="code"``.

Por ahora estoy escribiendo en restructured text, pero pretendo hacerlo mayormente en markdown y en jupyter notebooks. Finalmente nos queda subir el contenido generado a nuestro repositorio.

En mi caso uso rsync para copiar el sitio generado al repositorio:

::

    rsync --recursive blog/output/ repo/blog

Luego, add, commit, push y listo! 